import random as rdm

class School():

    def __init__(self, name):
        self.name = name


class People(School):

    def __init__(self, name, surname, position, age, mailbox):
        super().__init__(name)
        self.surname = surname
        self.position = position
        self.age = age
        self.mailbox = mailbox

    def __str__(self):
        return f'Name: {self.name}\n' \
               f'Surname: {self.surname}\n' \
               f'Position: {self.position}\n' \
               f'Age: {self.age}'

    def send_mail(self, person):
        content = input('Enter content od email:\n')
        person.mailbox.append(f'From: {self.position} - {self.name} {self.surname}\n{content}')


class Management(People):

    def __init__(self, name, surname, position, age, mailbox, leading_field, education):
        super().__init__(name, surname, position, age, mailbox)
        self.leading_field = leading_field
        self.education = education

    def __str__(self):
        return f'{super().__str__()}\n' \
               f'Leading field: {self.leading_field}\n' \
               f'Education: {self.education}'

    def send_mail(self, person):
        super().send_mail(person)


class Administration(People):
    def __init__(self, name, surname, position, age, mailbox, work_time):
        super().__init__(name, surname, position, age, mailbox)
        self.work_time = work_time

    def __str__(self):
        return f'{super().__str__()}\n' \
               f'Work time: {self.work_time}'

    def send_mail(self, person):
        super().send_mail(person)


class Teacher(People):
    def __init__(self, name, surname, position, age, mailbox, subject, class_teacher):
        super().__init__(name, surname, position, age, mailbox)
        self.subject = subject
        self.class_teacher = class_teacher

    def __str__(self):
        return f'{super().__str__()}\n' \
               f'Subject: {self.subject}\n' \
               f'Class teacher: {self.class_teacher}'

    def add_grade(self,student, grade):
        student.grades.append(grade)
        return student.grades

    def send_mail(self, person):
        super().send_mail(person)


class Student(People):

    def __init__(self, name, surname, position, age, mailbox, which_class, grades, prep_exam, exam_grade):
        super().__init__(name, surname, position, age, mailbox)
        self.which_class = which_class
        self.grades = grades
        self.prep_exam = prep_exam
        self.exam_grade = exam_grade

    def __str__(self):
        return f'{super().__str__()}\n' \
               f'Class: {self.which_class}\n' \
               f'Grades: {self.grades}\n' \
               f'Ready for the exam : {self.prep_exam}\n' \
               f'Exam result: {self.exam_grade} '

    def check_grades(self):
        average_grade = (sum(self.grades) / len(self.grades))
        print (f'Your grades: {self.grades}\n'
               f'Your average grade is {average_grade}')
        return average_grade

    def proceed_exam(self):
        if self.check_grades() >= 4:
            print('You can proceed to the exam.')
            Student.prep_exam = True
            chances = 51
            result = rdm.randint(0,100)
            if result <= chances:
                print('Congratulations! You PASS!')
                Student.exam_grade = "PASS"
            else:
                print('Bad luck. Try again in a while.')
        else:
            print('I\'m sorry, but Your grades average has to be greater or equal than 4.0')

    def send_mail(self, person):
        super().send_mail(person)


class Building(School):

    def __init__(self, name, people_capacity, surface, needs, equipment):
        super().__init__(name)
        self.people_capacity = people_capacity
        self.surface = surface
        self.needs = needs
        self.equipment = equipment

    def requirement(self, boss):
        if len(self.needs) > 0:
            boss.mailbox.append(f'From {self.name}\n We need {self.needs} ASAP.')

class Classroom(Building):
    def __init__(self, name, people_capacity, surface, needs, equipment):
        super().__init__(name, people_capacity, surface, needs, equipment)

    def requirement(self, boss):
        super().requirement(boss)


class UtilityRoom(Building):
    def __init__(self,name, people_capacity, surface, needs, equipment):
        super().__init__(name, people_capacity, surface, needs, equipment)

    def requirement(self, boss):
        super().requirement(boss)


class TeachingStaff(Building):
    def __init__(self,name, people_capacity, surface, needs, equipment):
        super().__init__(name, people_capacity, surface, needs, equipment)

    def requirement(self, boss):
        super().requirement(boss)


school = School('Primary school')

student1 = Student('John', 'Mnemonic', 'Student', 12,[], 6, [4,2,3], False, None)
teacher1 = Teacher('Mark', 'Padelacki', 'Teacher', 44, [], 'Math', '7th')
manager1 = Management('Joseph', 'Lothbrick', 'Main Manager', 38, [], 'Education management', 'Higher education')
administration1 = Administration('Alfred', 'Humiculus', 'Janitor', 48, [], 'Full-time')

classroom1 = Classroom('Class 01', 25, 40, ['projector'],['stationery', 'laptop', 'tablets', 'flipchart'] )
workshop1 = UtilityRoom('Basement workshop', 3, 10, None, ['complete tools', 'welder', 'soldering iron', 'drill'])
teachers_room = TeachingStaff('Teachers room', 15, 25, ['laptop', 'printer'], ['stationery', 'kettle', 'desks', 'interavtive screen'])
