import os
import tkinter as tk
import tkinter.font
from PIL import Image, ImageTk
from random import choice
import additional_functions as ad


class Menu():
    def __init__(self, window):
        self.logo = self.get_logo()
        self.window = window
        self.window.title("HANGMAN MENU")

        self.lbl_graphic = tk.Label(master=self.window, image=self.logo)
        self.lbl_graphic.grid(row=0, sticky="news", pady=10)

        self.frm_buttons = tk.Frame(master=self.window)
        self.frm_buttons.grid(row=1)

        self.btn_start = tk.Button(master=self.frm_buttons, text="START GAME", width=15, height=2,
                                   font=("Jokerman", 20), relief=tk.RAISED, borderwidth=10,
                                   command=self.start_game)
        self.btn_start.grid(row=0, column=0, padx=5, pady=5)
        self.btn_exit = tk.Button(master=self.frm_buttons, text="EXIT", width=15, height=2,
                                  font=("Jokerman", 20), relief=tk.RAISED, borderwidth=10,
                                  command=self.exit)
        self.btn_exit.grid(row=0, column=1, padx=5, pady=5)

        self.lbl_title_instr = tk. Label(master=self.window, text="Simple Game - Simple Rules",
                                         font=("Jokerman", 30))
        self.lbl_title_instr.grid(row=2, pady=20, padx=10)
        self.lbl_instructions = tk.Label(master=self.window, text=self.get_instructions(),
                                         font=("Jokerman", 15), justify="left",)
        self.lbl_instructions.grid(row=3, sticky="w", padx=10, pady=10)
        self.lbl_good_luck = tk.Label(master=self.window, text="GOOD LUCK !!!",
                                      font=("Jokerman", 30))
        self.lbl_good_luck.grid(row=4)

    def get_logo(self):
        path = os.getcwd()+"\graphics\logo.jpg"
        image = Image.open(path)
        photo = ImageTk.PhotoImage(image)
        return photo

    def get_instructions(self):
        text = "- You have to guess the mysterious word.\n" \
               "- Allowable amount of mistakes = 4.\n" \
               "- If you make the fifth mistake = GAME OVER.\n" \
               "- You can also try to guess whole password,\n" \
               "  but if you're wrong = as above."
        return text

    def start_game(self):
        ad.play_sound("click.wav")
        self.window.destroy()
        game_window = tk.Tk()
        Game(game_window, self)
        game_window.mainloop()

    def exit(self):
        ad.play_sound("exit.mp3")
        self.window.destroy()


class Game:
    def __init__(self, window, menu_instance): # added instance of Menu to Game to return to menu after playing.
        self.menu_instance = menu_instance
        self.mistakes = 0
        self.pictures = ad.pick_img_path()
        self.logo_image = self.draw_pic("logo", 400)
        self.mistakes_image = self.draw_pic(f"error_{self.mistakes}", 300)
        self.secret_words = ad.words_for_password()
        self.mysterious_word = choice(self.secret_words)
        self.utylity_letters = ad.letters_set()
        self.guesses_letters = []

        self.window = window
        self.window.title("HANGMAN GAME")

        self.frm_pic = tk.Frame(master=window)
        self.frm_pic.rowconfigure([0,1], weight=1)
        self.frm_pic.grid(row=0)

        self.frm_secret = tk.Frame(master=window)
        self.frm_secret.rowconfigure([0,1,2], weight=1)
        self.frm_secret.grid(row=1)

        self.frm_buttons = tk.Frame(master=window)
        self.frm_buttons.rowconfigure([0,1], weight=1)
        self.frm_buttons.grid(row=2)

        self.lbl_logo = tk.Label(master=self.frm_pic, image=self.logo_image)
        self.lbl_logo.grid(row=0, column=0)

        self.lbl_mistakes_pics = tk.Label(master=self.frm_pic, image = self.mistakes_image)
        self.lbl_mistakes_pics.grid(row=1,column=0)

        self.font_mysterious_word = tkinter.font.Font(family="Jokerman", size=20, weight="bold")
        self.lbl_mysterious_word = tk.Label(master=self.frm_secret, text=self.show_secret_word(),
                                            font=self.font_mysterious_word )
        self.lbl_mysterious_word.grid(row=1, column=0)

        self.ent_quess_word = tk.Entry(master=self.frm_secret, font=self.font_mysterious_word)
        self.ent_quess_word.grid(row=2, column=0, pady=10)

        self.btn_guess_word = tk.Button(master = self.frm_secret, text="Try to guess password",
                                        command=self.trying_guess, width=20, height=2)
        self.btn_guess_word.grid(row=3, column=0, pady=10)

        for column, letter in enumerate(self.utylity_letters):
            row = 0
            if column >= 13:
                row = 1
                column = column - 13
            btn = tk.Button(master=self.frm_buttons, height=2, width=2, text=letter, bg="grey", fg="black",
                            relief=tk.RAISED, borderwidth=5)
            btn.config(command=lambda b=btn, l=letter: self.click_letter_button(l, b))
            btn.grid(row=row, column=column, sticky="ew", padx=5, pady=10)


    def draw_pic(self, name, size):
        image = Image.open(self.pictures[name])
        width, height = image.size
        new_width = size
        new_height = int((new_width / width) * height)
        image = image.resize((new_width, new_height))
        photo = ImageTk.PhotoImage(image)
        return photo

    def show_secret_word(self):
        player_board = []
        for letter in self.mysterious_word:
            if letter in self.guesses_letters:
                player_board.append(letter + " ")
            else:
                player_board.append("_ ")
        player_board = ''.join(player_board)
        return player_board

    def win(self):
        ad.play_sound("win.mp3")
        self.ent_quess_word.destroy()
        self.btn_guess_word.destroy()
        self.lbl_mysterious_word.config(text="CONGRATULATIONS!\n\n"
                                             f"The mysterious word is:\n{self.mysterious_word}\n\n"
                                             "YOU WIN!",
                                        font=("Jokerman", 15),
                                        justify="center")

        self.window.after(3000, self.exit)

    def lost(self):
        self.ent_quess_word.destroy()
        self.btn_guess_word.destroy()
        self.lbl_mysterious_word.config(text="You should shame!\n"
                                             "Hangman was killed because of you!\n\n"
                                             f"The mysterious word is:\n{self.mysterious_word}\n\n"
                                             "YOU LOST!",
                                        font=("Jokerman", 15),
                                        justify="center")

        self.window.after(3000, self.exit)

    def trying_guess(self):
        attempt = self.ent_quess_word.get()
        attempt = attempt.upper()
        if attempt == self.mysterious_word:
            self.win()
        else:
            ad.play_sound("game_over.mp3")
            self.lost()

    def btn_letter_add(self, letter):
        self.guesses_letters.append(letter)
        self.lbl_mysterious_word.config(text=self.show_secret_word())

    def btn_letter_check(self, letter):
        if not letter in self.mysterious_word:
            if self.mistakes != 4:
                ad.play_sound("missed.wav")
            else:
                ad.play_sound("game_over.mp3")
            self.mistakes += 1
            self.mistakes_image = self.draw_pic(f"error_{self.mistakes}", 250)
            self.lbl_mistakes_pics.config(image=self.mistakes_image)
        else:
            ad.play_sound("guessed.wav")

    def btn_letter_errors_limit(self):
        if self.mistakes == 5:
            self.lost()


    def btn_win_check(self):
        text = self.lbl_mysterious_word.cget("text")
        text = text.replace(" ", "")
        if self.mysterious_word == text:
            self.win()

    def btn_letter_disable(self, button):
        button.config(state=tk.DISABLED)

    def click_letter_button(self, letter, button):
        self.btn_letter_add(letter)
        self.btn_letter_check(letter)
        self.btn_letter_errors_limit()
        self.btn_win_check()
        self.btn_letter_disable(button)

    def exit(self):
        self.window.destroy()
        menu_window = tk.Tk()
        Menu(menu_window)
        menu_window.mainloop()
