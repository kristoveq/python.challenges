import os
from pathlib import Path
import string
import pygame

def pick_img_path():
    current_path = Path.cwd() / "graphics"
    image_paths = []
    pics = {}

    for one in current_path.iterdir():
        if one.suffix == ".jpg":
            image_paths.append(one)

    count = 0
    for one in image_paths:
        if one.stem =="logo":
            pics.setdefault("logo", f"{one}")
        else:
            pics.setdefault(f"error_{count}", f"{one}")
            count += 1
    return pics

def letters_set():
    letters = string.ascii_uppercase
    return letters

def words_for_password():
    words_path = Path.cwd() / "words.txt"
    with open(words_path, 'r') as words_file:
        words = words_file.readlines()

    words = words[0].split(',')
    index = 0
    for word in words:
        words[index] = word.upper().strip()
        index += 1
    return words

def play_sound(name_of_sound):
    pygame.init()
    file_path = Path.cwd() / "sounds" / name_of_sound
    pygame.mixer.init()
    pygame.mixer.music.load(file_path)
    pygame.mixer.music.play()

    while pygame.mixer.music.get_busy():
        pygame.time.Clock()

    pygame.quit()



