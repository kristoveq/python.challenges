import pathlib

extensions = ['.png', '.jpg', '.gif']
working_dir = pathlib.Path.cwd() / 'move_images' / 'practise_files'

new_folder_dir = working_dir / 'images'
new_folder_dir.mkdir(exist_ok=True)

for one in working_dir.rglob('*.*'):
    if one.suffix in extensions:
        source = one
        destination = new_folder_dir / one.name
        source.replace(destination)