import random

def simulate_election():
    regions_percentage_A = [0.87, 0.65, 0.17]
    cand_A = 0
    cand_B = 0
    for region in regions_percentage_A:
        vote = random.randint(1, 100)
        result = vote / 100
        if result < region:
            cand_A += 1
        else:
            cand_B += 1
    if cand_A >= 2:
        return 'A'
    else:
        return 'B'

candidate_A = 0
candidate_B = 0
for x in range(1, 10000):
    if simulate_election() == 'A':
        candidate_A += 1
    else:
        candidate_B += 1
print(f'After deep research, The Junior Institute concludes that Candidate "A" wins election '
      f'{candidate_A/100:.2f}% of votes.\n'
      f'Candidate B gain only {candidate_B/100:.2f}% of votes.')
