from pathlib import Path
import csv

def int_convert(data):
    data = int(data)
    return data

def del_duplicated(list):
    for element in list:
        if list.count(element) > 1:
            list.remove(element)
    return list

scores = []
high_scores = []
file_path = Path.cwd() / 'challenge_scores' / 'scores.csv'
new_path = Path.cwd() / 'challenge_scores' / 'high_scores.csv'

with open(file_path, mode='r', encoding='utf-8', newline='') as file:
    reader = csv.DictReader(file)
    for row in reader:
        scores.append(row)

for person in scores:
    person['score'] = int_convert(person['score'])

del_duplicated(scores)

for score in scores:
    high_scores.append({'name' : score['name'], 'high_score' : score['score']})

for score in scores:
    for result in high_scores:
        if score['name'] == result['name'] and score['score'] > result['high_score']:
            high_scores.remove(result)

with open(new_path, mode='w', encoding='utf-8', newline='') as file:
    writer = csv.DictWriter(file, fieldnames=['name', 'high_score'])
    writer.writeheader()
    writer.writerows(high_scores)