import random
import time

class Animal:
    pos = 0
    def __init__(self, name, type, sex, flying, food, age, length_of_life, reproductive_time, average_price):
        self.name = name
        self.type = type
        self.sex = sex
        self.flying = flying
        self.food = food
        self.age = age
        self.length_of_life = length_of_life
        self.reproductive_time = reproductive_time
        self.average_price = average_price

    def __str__(self):
        return f'Animal name: {self.name.capitalize()},\n' \
               f'Type: {self.type}\n' \
               f'Sex: {self.sex},\n' \
               f'Can fly: {self.flying},\n' \
               f'Food used: {self.food},\n' \
               f'Age: {self.age},\n' \
               f'Average length of life: {self.length_of_life},\n' \
               f'Reproductive time: {self.reproductive_time},\n' \
               f'Average price for 1 {self.sex} is: {self.average_price} PLN.'

    def talk(self, sound):
        if sound is None:
            return '...'
        else:
            return f'{self.name.capitalize()} says {sound}.'

    def check_position(self):
        return f'Current position {self.name.capitalize()} is {self.pos} meters from You.'

    def walk(self, distance):
        self.pos = self.pos + distance
        return f'{self.name.capitalize()} move on!\n' \
               f'OMG...i scare {self.name.capitalize()}. It moves on by {distance} meters.'

    def technical_conditions(self, extra, price):
        return f'In breeding {self.type}, the additional cost of {extra} should be taken: about {price} PLN.'


class Geese(Animal):
    sounds = ['\'honk, honk !\'', '...']
    def talk(self, sound = random.choice(sounds)):
            return super().talk(sound)

    def technical_conditions(self, extra='vaccine', price=50):
        return super().technical_conditions(extra, price)

    def walk(self, distance=7):
        return super().walk(distance)


class Pigs(Animal):
    def talk(self, sound = 'qui, qui !'):
        return super().talk(sound)

    def technical_conditions(self, extra='hormonal support', price=250):
        return super().technical_conditions(extra, price)

    def walk(self, distance=3):
        return super().walk(distance)


class Cows(Animal):
    def talk(self, sound = 'mooo !'):
        return super().talk(sound)

    def technical_conditions(self, extra = 'milkmaid', price=1000):
        return super().technical_conditions(extra, price)

    def walk(self, distance=2):
        return super().walk(distance)

goose = Geese('goosy','bird', 'female', True, 'corn', 2, 12, '2 months', 170)
piggy = Pigs('piggy', 'mammal', 'male', False, 'feed mixture', 4, 18, '115 days', 1800 )
spotty = Cows('spotty', 'mammal', 'female', False, 'forage', 2, 17, '280 days', 5500)

'--------------------------------------------------------------------------------------'
# This is result of my playing with the program.
# You can just ignore it :)
list_of_animals = [goose, piggy, spotty]

def transaction(animal):
    print('Hi Buyer! How can I help You?')
    time.sleep(1)
    print(f'I want to buy {animal.name}.')
    time.sleep(1)
    print(f'Do you hear {animal.name}?')
    time.sleep(1)
    if animal.talk() == '...':
        print(f'- {animal.name.capitalize()}: {animal.talk()}')
        time.sleep(1)
        print(f'Oh no...she\'s sick! If you wanna buy {animal.name} You have to be additionaly prepared.\n'
              f'{animal.technical_conditions()}\n')
        time.sleep(2)
    else:
        print(animal.talk())
        time.sleep(1)
        print(animal.technical_conditions())
        time.sleep(2)
    print(animal)
#transaction(random.choice(list_of_animals))