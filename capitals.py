import random

capitals_dict = {
'Alabama': 'Montgomery',
'Alaska': 'Juneau',
'Arizona': 'Phoenix',
'Arkansas': 'Little Rock',
'California': 'Sacramento',
'Colorado': 'Denver',
'Connecticut': 'Hartford',
'Delaware': 'Dover',
'Florida': 'Tallahassee',
'Georgia': 'Atlanta',
}

state, capital = random.choice(list(capitals_dict.items()))
print('Type "exit" if You want to quit.\n ')

while True:
    user = input(f'What is the capital of {state}?\n: ')
    if user.lower() == 'exit':
        print('Goodbye.')
        break
    elif user.lower() == capital.lower():
        print('Correct.')
        break
    else:
        print('Incorrect answer. Try one more time.')
        continue

