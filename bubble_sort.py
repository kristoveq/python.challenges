from random import sample

global_data = [x for x in range(1, 1000 +1)]
data = sample(global_data, k=20)

# data = [1,2,4,4,6,3,7,9,3]

def bubble_sorting(values):
    disorder = True
    print("Unsorted data:\t " + ", ".join(str(number) for number in values))

    while disorder == True:
        disorder = False
        counter = 0
        for num in values:
            if counter == len(values) -1:
                break
            if values[counter] > values[counter + 1]:
                disorder = True
                values[counter], values[counter + 1] = values[counter +1], values[counter]
            counter += 1

    print("Result of Bubble Sort:\t " + ", ".join(str(number) for number in values))
    return values

bubble_sorting(data)