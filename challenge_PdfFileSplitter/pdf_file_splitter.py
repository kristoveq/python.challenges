from pathlib import Path
from PyPDF2 import PdfWriter, PdfReader

class PDFFileSplitter:
    def __init__(self, file_path):
        self.pdf_reader = PdfReader(file_path)
        self.writer1 = None
        self.writer2 = None

    def split(self, breakpoint):
        self.writer1 = PdfWriter()
        for page in self.pdf_reader.pages[: breakpoint]:
            self.writer1.add_page(page)

        self.writer2 = PdfWriter()
        for page in self.pdf_reader.pages[breakpoint :]:
            self.writer2.add_page(page)

    def write(self, file_name):
        writers = [pdf_splitter.writer1, pdf_splitter.writer2]
        counter = 1
        for writer in writers:
            with Path(f'{file_name}_{counter}.pdf').open(mode='wb') as output_file:
                writer.write(output_file)
            counter += 1

pdf_splitter = PDFFileSplitter(Path.cwd() / 'main_file.pdf')
pdf_splitter.split(150)
pdf_splitter.write('splitted_file')
