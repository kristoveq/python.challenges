amount = input('Enter initial amount: ')
amount = float(amount)
rate = input('Enter annual percentage(%) rate: ')
rate = float(rate) * 0.01
years = input('Number of years in deposit: ')
years = int(years)

def invest(amount, rate, years):
    count = 1
    while count <= years:
        result = amount + (amount * rate)
        print(f'year {count}: $ {result:.2f}')
        amount = result
        count += 1

invest(amount, rate, years)