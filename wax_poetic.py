import random

nouns = ["fossil", "horse", "aardvark", "judge", "chef", "mango",
"extrovert", "gorilla"]
verbs = ["kicks", "jingles", "bounces", "slurps", "meows",
"explodes", "curdles"]
adjectives = ["furry", "balding", "incredulous", "fragrant",
"exuberant", "glistening"]
prepositions = ["against", "after", "into", "beneath", "upon",
"for", "in", "like", "over", "within"]
adverbs = ["curiously", "extravagantly", "tantalizingly",
"furiously", "sensuously"]

def poem():
    noun = random.choices(nouns, k=3)
    verb = random.choices(verbs, k=3)
    adj = random.choices(adjectives, k=3)
    prep = random.choices(prepositions, k=2)
    adverb = random.choices(adverbs, k=1)
    vowels = 'aeiouy'

    for letter in vowels:
        if adj[0].startswith(letter):
            art = 'an'
            break
        else:
            art = 'a'

    poem = (f'{art} {adj[0]} {noun[0]}\n'
    f'{art} {adj[0]} {noun[0]} {verb[0]} {prep[0]} the {adj[1]} {noun[1]}\n'
    f'{adverb[0]}, the {noun[0]} {verb[1]}\n'
    f'the {noun[1]} {verb[2]} {prep[1]} a {adj[2]} {noun[2]}')


    return poem

print(poem())