import tkinter as tk
from tkinter.filedialog import asksaveasfilename
from random import randint

window = tk.Tk()
window.title("Make your own poem!")

def generate_poem():
    nouns = ent_nouns.get().split(',')
    verbs = ent_verbs.get().split(',')
    adjectives = ent_adjectives.get().split(',')
    prepositions = ent_prepositions.get().split(',')
    adverbs = ent_adverbs.get().split(',')
    word_list = [nouns, verbs, adjectives, prepositions]

    for words in word_list:
        if len(words) != 3 or len(adverbs) != 1:
            lbl_poem['text'] = "ERROR\n" \
                               "Stick to a specific word count !"
            return

    for words in word_list:
        for word in words:
            if words.count(word) > 1:
                lbl_poem['text'] = "ERROR\n" \
                                   "Don't use same words!"
                return

    noun_1 = nouns.pop(randint(0, len(nouns) - 1))
    noun_2 = nouns.pop(randint(0, len(nouns) - 1))
    noun_3 = nouns.pop(randint(0, len(nouns) - 1))

    verb_1 = verbs.pop(randint(0, len(verbs) - 1))
    verb_2 = verbs.pop(randint(0, len(verbs) - 1))
    verb_3 = verbs.pop(randint(0, len(verbs) - 1))

    adj_1 = adjectives.pop(randint(0, len(adjectives) - 1))
    adj_2 = adjectives.pop(randint(0, len(adjectives) - 1))
    adj_3 = adjectives.pop(randint(0, len(adjectives) - 1))

    prep_1 = prepositions.pop(randint(0, len(prepositions) - 1))
    prep_2 = prepositions.pop(randint(0, len(prepositions) - 1))
    prep_3 = prepositions.pop(randint(0, len(prepositions) - 1))

    adv_1 = adverbs.pop(randint(0, len(adverbs) - 1))

    if adj_1 in "aiuoe":
        art = "An"
    else:
        art = "A"

    result = f"{art} {adj_1} {noun_1}\n\n" \
             f"A {adj_1} {noun_1} {verb_1} {prep_1} the {adj_2} {noun_2}\n" \
             f"{adv_1}, the {noun_1} {verb_2}\n" \
             f"the {noun_2} {verb_3} {prep_2} a {adj_3} {noun_3}"

    lbl_poem['text'] = result

def save_poem():
    save_path = asksaveasfilename(
        defaultextension="text",
        filetypes=[("Text Files", "*.txt"), ("All Files", "*.*")]
    )
    if not save_path:
        return
    with open(save_path, 'w') as output_file:
        poem = lbl_poem["text"]
        output_file.write(poem)


lbl_description = tk.Label(
    master=window,
    text="Enter your favorite words, separated by comas."
)
lbl_description.grid(sticky="n", pady=10)

frm_ingredients = tk.Frame(master=window)

ent_nouns = tk.Entry(master=frm_ingredients, width=100)
lbl_nouns = tk.Label(master=frm_ingredients, text="Nouns (x3):")
lbl_nouns.grid(row=0, column=0, sticky="e")
ent_nouns.grid(row=0, column=1, sticky="w")

ent_verbs = tk.Entry(master=frm_ingredients, width=100)
lbl_verbs = tk.Label(master=frm_ingredients, text="Verbs (x3):")
lbl_verbs.grid(row=1, column=0, sticky="e")
ent_verbs.grid(row=1, column=1, sticky="w")

ent_adjectives = tk.Entry(master=frm_ingredients, width=100)
lbl_adjectives = tk.Label(master=frm_ingredients, text="Adjectives (x3):")
lbl_adjectives.grid(row=2, column=0, sticky="e")
ent_adjectives.grid(row=2, column=1, sticky="w")

ent_prepositions = tk.Entry(master=frm_ingredients, width=100)
lbl_prepositions = tk.Label(master=frm_ingredients, text="Prepositions (x3):")
lbl_prepositions.grid(row=3, column=0, sticky="e")
ent_prepositions.grid(row=3, column=1, sticky="w")

ent_adverbs = tk.Entry(master=frm_ingredients, width=100)
lbl_adverbs = tk.Label(master=frm_ingredients, text="Adverbs (x1) :")
lbl_adverbs.grid(row=4, column=0, sticky="e")
ent_adverbs.grid(row=4, column=1, sticky="w")

frm_ingredients.grid(row=1, column=0)

btn_generate = tk.Button(master=window, text="Generate", command=generate_poem)
btn_generate.grid(row=2, column=0, pady=10, padx=10)

frm_result = tk.Frame(master=window, relief=tk.GROOVE, borderwidth=5)
frm_result.rowconfigure(0, weight=1)
frm_result.columnconfigure(0, weight=1)

lbl_poem = tk.Label(master=frm_result, text="Your poem:\n")
lbl_poem.grid(pady=10, padx=10)

btn_save = tk.Button(master=frm_result, text="Save to file", command=save_poem)
btn_save.grid(pady=10)

frm_result.grid(row=4, column=0, sticky="news", padx=5, pady=5)
window.mainloop()