import pygame
from random import choice

pygame.init()
pygame.display.set_caption("POONG")
screen = pygame.display.set_mode((1280, 800))
clock = pygame.time.Clock()

is_running = False
current_background = None

class Theme():
    def __init__(self):

        self.background = pygame.image.load("graphics/theme_bg.png").convert()
        self.difficulty = None
        self.selected_option = 0

        self.theme_font = pygame.font.Font("doom_font.ttf", 200)
        self.game_name = self.theme_font.render("P O O N G", True, (255, 255, 255))
        self.game_name_rect = self.game_name.get_rect(center=(-350, 200))

        self.level_font = pygame.font.Font("doom_font.ttf", 100)
        self.level_easy = self.level_font.render("e a s y", True, (100, 255, 0))
        self.selected_level_easy = self.level_font.render("e a s y", True, (100, 255, 0))
        self.level_easy_rect = self.level_easy.get_rect(center=(640, 450))

        self.level_medium = self.level_font.render("m e d i u m", True, (255, 255, 255))
        self.selected_level_medium = self.level_font.render("m e d i u m", True, (100, 255, 0))
        self.level_medium_rect = self.level_medium.get_rect(center=(640, 550))

        self.level_hard = self.level_font.render("h a r d", True, (255, 255, 255))
        self.selected_level_hard = self.level_font.render("h a r d", True, (100, 255, 0))
        self.level_hard_rect = self.level_hard.get_rect(center=(640, 650))

    def move_title(self):
        theme.game_name_rect.x += 2
        if theme.game_name_rect.x >= 370:
            theme.game_name_rect.x = 370
            self.set_buttons()

    def set_buttons(self):
        screen.blit(self.level_easy, self.level_easy_rect)
        screen.blit(self.level_medium, self.level_medium_rect)
        screen.blit(self.level_hard, self.level_hard_rect)

    def choose_difficulty(self):
        options = [self.level_easy, self.level_medium, self.level_hard]
        click_button = pygame.mixer.Sound('sounds/click_button.mp3')
        click_button.set_volume(1.0)

        if event.key == pygame.K_DOWN:
            click_button.play()
            self.selected_option = (self.selected_option + 1) % len(options)
        elif event.key == pygame.K_UP:
            click_button.play()
            self.selected_option = (self.selected_option - 1) % len(options)
        elif event.key == pygame.K_SPACE:
            click_button.play()
            return options[self.selected_option]

        if self.selected_option == 0:
            self.level_easy = self.level_easy = self.level_font.render("e a s y", True, (100, 255, 0))
            self.level_medium = self.level_font.render("m e d i u m", True, (255, 255, 255))
            self.level_hard = self.level_font.render("h a r d", True, (255, 255, 255))
        elif self.selected_option == 1:
            self.level_easy = self.level_easy = self.level_font.render("e a s y", True, (255, 255, 255))
            self.level_medium = self.level_font.render("m e d i u m", True, (100, 255, 0))
            self.level_hard = self.level_font.render("h a r d", True, (255, 255, 255))
        else:
            self.level_easy = self.level_easy = self.level_font.render("e a s y", True, (255, 255, 255))
            self.level_medium = self.level_font.render("m e d i u m", True, (255, 255, 255))
            self.level_hard = self.level_font.render("h a r d", True, (100, 255, 0))

    def reset_theme(self):
        self.selected_option = 0
        self.game_name_rect.x -= 1000
        self.level_easy = self.level_font.render("e a s y", True, (100, 255, 0))
        self.level_medium = self.level_font.render("m e d i u m", True, (255, 255, 255))
        self.level_hard = self.level_font.render("h a r d", True, (255, 255, 255))

theme = Theme()

class Player(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.image = pygame.image.load("graphics/player_paddle.png").convert_alpha()
        self.x = 640
        self.y = 790
        self.rect = self.image.get_rect(midbottom = (self.x, self.y))
        self.points = 0
        self.control = True

    def player_input(self):
        if self.control == True:
            keys = pygame.key.get_pressed()
            if keys[pygame.K_LEFT] and self.rect.left > 0:
                self.x -= 10
            elif keys[pygame.K_RIGHT] and self.rect.right < 1280:
                self.x += 10
            self.rect.midbottom = (self.x, self.y)

    def update(self):
        self.player_input()


class Enemy(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.image = pygame.image.load("graphics/enemy_1.png").convert_alpha()
        self.x = 640
        self.y = 50
        self.rect = self.image.get_rect(midbottom=(self.x, self.y))
        self.points = 0
        self.speed = None

    def fetching(self):
        if (self.rect.x - ball.sprite.rect.x) > 3 and self.rect.left > 0:
            self.rect.x -= self.speed
        elif (ball.sprite.rect.x - self.rect.x) > 3 and self.rect.right < 1280:
            self.rect.x += self.speed

    def update(self):
        self.fetching()


class Ball(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.image = pygame.image.load("graphics/ball.png").convert_alpha()
        self.rect = self.image.get_rect(center = (choice([x for x in range (100, 1200)]),100))
        self.speed_x = 2
        self.speed_y = 5
        self.angle = 10

    def moving(self):
        self.rect.x += self.speed_x
        self.rect.y += self.speed_y

    def rotating(self): # fucked up!  >:[
        rotated_ball = pygame.transform.rotate(self.image, self.angle)
        primal_center = self.rect.center
        self.angle += 10
        return screen.blit(rotated_ball, rotated_ball.get_rect(center = primal_center).topleft)

    def edges_collide(self):
        if self.rect.x >= 1250 or self.rect.x <= 0:
            self.speed_x *= -1

    def paddle_collide(self):
        for paddle in [player.sprite.rect, enemy.sprite.rect]:
            if ball.sprite.rect.colliderect(paddle):

                bounce = pygame.mixer.Sound('sounds/bouncing.wav')
                bounce.set_volume(1.0)
                bounce.play()

                if self.speed_y > 0:
                    self.speed_y += 0.5
                else:
                    self.speed_y -= 0.5
                self.speed_y *= -1

                if ball.sprite.rect.center[0] in range(paddle.left, paddle.center[0] - 25):
                    self.speed_x -= 2
                elif ball.sprite.rect.center[0] in range(paddle.center[0] + 25, paddle.right):
                    self.speed_x += 2

    def add_points(self):
        if  ball.sprite.rect.bottom < -100 or ball.sprite.rect.top > 900:

            point_sound = pygame.mixer.Sound('sounds/point.wav')
            point_sound.set_volume(1.0)
            point_sound.play()

            if ball.sprite.rect.bottom < -100:
                player.sprite.points += 1
            elif ball.sprite.rect.top > 900:
                enemy.sprite.points += 1
            show_results()
            player.sprite.control = True
            disturber.empty()
            self.speed_x = 2
            self.speed_y = 5
            self.rect = self.image.get_rect(center=(choice([x for x in range(100, 1200)]), 100))

    def update(self):
        self.rotating()
        self.moving()
        self.edges_collide()
        self.paddle_collide()
        self.add_points()


class Disturbers(pygame.sprite.Sprite):
    def __init__(self, type):
        super().__init__()
        if type == "grenade":
            self.image = pygame.image.load("graphics/grenade.png").convert_alpha()
        elif type == "molotov":
            self.image = pygame.image.load("graphics/molotov.png").convert_alpha()
        else:
            self.image = pygame.image.load("graphics/bomb.png").convert_alpha()
        self.explode_animation_1 = pygame.image.load("graphics/explode_1.png")
        self.explode_animation_2 = pygame.image.load("graphics/explode_2.png")
        self.explode_animation_3 = pygame.image.load("graphics/explode_3.png")
        self.explode_animation_4 = pygame.image.load("graphics/explode_4.png")
        self.speed_y = 5
        self.angle = -10
        self.rect = self.rect = self.image.get_rect(center = (choice([x for x in range (1280)]),50))
        self.collided = False
        self.collision_time = 0

    def shoot(self):
        self.rect.y += self.speed_y

    def collission(self):
        if self.rect.colliderect(player.sprite.rect) and not self.collided:

            explode = pygame.mixer.Sound('sounds/collision.wav')
            explode.set_volume(1.0)
            explode.play()

            player.sprite.control = False
            self.collision_time = pygame.time.get_ticks()
            self.collided = True

    def animation(self):
        if pygame.time.get_ticks() - self.collision_time >= 50:
            animation_parts = [self.explode_animation_1, self.explode_animation_2,
                               self.explode_animation_3, self.explode_animation_4]
            index = (pygame.time.get_ticks() - self.collision_time) // 50 % len(animation_parts)
            self.image = animation_parts[index].convert_alpha()
            pygame.display.update()

    def destroy(self):
        if self.rect.y >= 1400:
            self.kill()

    def update(self):
        self.shoot()
        if self.collided:
            self.animation()
            self.speed_y = 0
            if pygame.time.get_ticks() - self.collision_time >= 1000:
                self.kill()
                player.sprite.control = True
                pygame.display.update()
        self.collission()
        self.destroy()


def difficulty_medium():
    if player.sprite.points == 2:
        enemy.sprite.speed = 7
    elif player.sprite.points == 3:
        enemy.sprite.speed = 8
    elif player.sprite.points == 4:
        enemy.sprite.speed = 9
    elif player.sprite.points == 4:
        enemy.sprite.speed = 10

def difficulty_hard():
    disturber.add(Disturbers(choice(['grenade', 'molotov', 'bomb'])))
    if player.sprite.points == 1:
        pygame.time.set_timer(disturber_timer, 2000)
    elif player.sprite.points == 2:
        pygame.time.set_timer(disturber_timer, 1500)
    elif player.sprite.points == 3:
        enemy.sprite.speed = 8
        pygame.time.set_timer(disturber_timer, 1000)
    elif player.sprite.points == 4:
        enemy.sprite.speed = 8
        pygame.time.set_timer(disturber_timer, 500)

def game_set(enemy_img, enemy_speed, bg_img):
    theme.reset_theme()
    enemy.sprite.image = pygame.image.load(enemy_img).convert_alpha()
    enemy.sprite.speed = enemy_speed
    return pygame.image.load(bg_img).convert()

def show_results():
    points_font = pygame.font.Font("doom_font.ttf", 100)

    enemy_points_table = points_font.render(f"ENEMY : {enemy.sprite.points}", True, (255,255,255))
    enemy_points_rect = enemy_points_table.get_rect(center = (640, 300))

    player_points_table = points_font.render(f"PLAYER : {player.sprite.points}", True, (255,255,255))
    player_points_rect = player_points_table.get_rect(center = (640, 500))

    screen.blit(enemy_points_table, enemy_points_rect)
    screen.blit(player_points_table, player_points_rect)

    pygame.display.update()
    pygame.time.delay(2000)
    disturber.empty()

def end_round():
    if player.sprite.points == 5 or enemy.sprite.points == 5:
        pygame.mixer_music.stop()
        if player.sprite.points == 5:
            win_sound = pygame.mixer.Sound('sounds/victory.mp3')
            win_sound.set_volume(1.0)
            win_sound.play()
            announcement = "YOU WIN"
        elif enemy.sprite.points == 5:
            lose_sound = pygame.mixer.Sound('sounds/game_over.mp3')
            lose_sound.set_volume(1.0)
            lose_sound.play()
            announcement = "GAME OVER"

        pygame.time.set_timer(disturber_timer, 1500)
        player.sprite.points = 0
        enemy.sprite.points = 0
        theme.difficulty = None

        text_table = theme.theme_font.render(announcement, True, (255,255,255))
        text_rect = text_table.get_rect(center = (640, 400))
        screen.fill("Black")
        screen.blit(text_table, text_rect)
        pygame.display.update()
        pygame.time.delay(3500)
        pygame.mixer.music.stop()
        disturber.empty()
        return False
    else:
        return True

# Groups
player = pygame.sprite.GroupSingle()
player.add(Player())

enemy = pygame.sprite.GroupSingle()
enemy.add(Enemy())

ball = pygame.sprite.GroupSingle()
ball.add(Ball())

disturber = pygame.sprite.Group()

# Timer
disturber_timer = pygame.USEREVENT + 1
pygame.time.set_timer(disturber_timer, 1500)

while True:
    start_time = pygame.time.get_ticks()

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            exit()

        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE:
                pygame.quit()
                exit()

            elif is_running == False and theme.game_name_rect.x == 370:
                selected_option = theme.choose_difficulty()

                if selected_option == theme.level_easy:
                    theme.difficulty = "easy"
                    current_background = game_set("graphics/enemy_1.png", 5, "graphics/easy_bg.png")
                    is_running = True
                    pygame.mixer.music.load("sounds/easy_music.mp3")
                    pygame.mixer.music.set_volume(0.3)
                    pygame.mixer.music.play(-1)


                elif selected_option == theme.level_medium:
                    theme.difficulty = "medium"
                    current_background = game_set("graphics/enemy_2.png", 6, "graphics/medium_bg.png")
                    is_running = True
                    pygame.mixer.music.load("sounds/medium_music.mp3")
                    pygame.mixer.music.set_volume(0.3)
                    pygame.mixer.music.play(-1)

                elif selected_option == theme.level_hard:
                    theme.difficulty = "hard"
                    current_background = game_set("graphics/enemy_3.png", 7, "graphics/hard_bg.png")
                    is_running = True
                    pygame.mixer.music.load("sounds/hard_music.mp3")
                    pygame.mixer.music.set_volume(0.3)
                    pygame.mixer.music.play(-1)

        if theme.difficulty == "medium":
            difficulty_medium()
        elif theme.difficulty == "hard" and event.type == disturber_timer:
            difficulty_hard()

    if is_running:
        screen.blit(current_background, (0,0))

    else:
        if not pygame.mixer.music.get_busy():
            pygame.mixer.music.load("sounds/theme_music.mp3")
            pygame.mixer.music.play(0)

        current_background = theme.background
        screen.blit(current_background, (0,80))
        theme.move_title()
        screen.blit(theme.game_name, theme.game_name_rect)

    if is_running:
        player.draw(screen)
        player.update()

        enemy.draw(screen)
        enemy.update()

        ball.draw(screen)
        ball.update()

        disturber.draw(screen)
        disturber.update()

        is_running = end_round()

    pygame.display.update()
    clock.tick(60)
