import random

def instructions():
    print('''
   _    _                                         
  | |  | |                                        
  | |__| | __ _ _ __   __ _ _ __ ___   __ _ _ __  
  |  __  |/ _` | '_ \ / _` | '_ ` _ \ / _` | '_ \ 
  | |  | | (_| | | | | (_| | | | | | | (_| | | | |
  |_|  |_|\__,_|_| |_|\__, |_| |_| |_|\__,_|_| |_|
                       __/ |                      
                      |___/                       ''')
    print('Welcome in Hangman game. The rules are really simple!\n'
      '- Select mode.\n'
      '- Type one by one LETTERS or guess password.\n'
      '- You have only 5 chances for mistake.\n'
      '- Sixth mistake = GAME OVER.')
    print('*' * 50)
    print()


def win(main_word):
    print('''
 __     __          __          ___       
 \ \   / /          \ \        / (_)      
  \ \_/ /__  _   _   \ \  /\  / / _ _ __  
   \   / _ \| | | |   \ \/  \/ / | | '_ \ 
    | | (_) | |_| |    \  /\  /  | | | | |
    |_|\___/ \__,_|     \/  \/   |_|_| |_|                
                                          ''')
    print('\nCongratulations!\n'
              'You WIN!\n'
              f'The password is: {main_word}')
    return True


def error(error_number):
    errors = {'error_1' : '''





      |
     / \\''',
              'error_2' : '''
      |--
      |
      |
      |
      |
      |
     / \\''',
              'error_3' : '''
      |------|
      |      |
      |
      |
      |
      |
     / \\''',
              'error_4' : '''
      |------|
      |      |
      |      0
      |     \|/
      |     
      |
     / \\''',
              'error_5' : '''
      |------|
      |      |
      |      0
      |     \|/
      |     / \\
      |
     / \\''',
              'error_6' : '''
      _____          __  __ ______    ______      ________ _____  
     / ____|   /\   |  \/  |  ____|  / __ \ \    / /  ____|  __ \ 
    | |  __   /  \  | \  / | |__    | |  | \ \  / /| |__  | |__) |
    | | |_ | / /\ \ | |\/| |  __|   | |  | |\ \/ / |  __| |  _  / 
    | |__| |/ ____ \| |  | | |____  | |__| | \  /  | |____| | \ \ 
     \_____/_/    \_\_|  |_|______|  \____/   \/   |______|_|  \_\ '''}
    print(errors.get(f'error_{error_number}'))


def mode(list_of_passwords):
    while True:
        user = input('Select mode:\n'
                     '1.You enter single-word password.\n'
                     '2.Password is randomly drawn from the internal database.\n'
                     '              * You can also type "exit" to end program.\n'
                     ':\t')
        if user.lower() == 'exit':
            return user.lower()

        try:
            user = int(user)
        except ValueError:
            print('To select mode you have to type a NUMBER: "1" or "2".\n')
            continue

        if user == 1:
            password = input('Enter password: ')
            if password.isalpha():
                return password
            else:
                print('Password should be single-word (consisting of letters only!)\n')
        elif user == 2:
            password = random.choice(list_of_passwords)
            return password
        else:
            print('To select mode you have to type a NUMBER: "1" or "2".\n')


def player_guess(guesses_letters):
    while True:
        letter = input('Type letter or guess password: ').lower()
        if not letter.isalpha():
            print('Type only letters!')
        elif letter in guesses_letters:
            print('You\'ve already type it!\n'
                  'Pick another one.\n')
        else:
            guesses_letters.append(letter)
            return letter


def check_letter(main_word, guesses_letters):
    player_board = []
    for letter in main_word:
        if letter in guesses_letters:
            player_board.append(letter)
        else:
            player_board.append('_')
    player_board = ''.join(player_board)
    return player_board


def guess_word(letter, main_word):
    if letter == main_word:
        return True
    else:
        return False

def game():
    list_of_passwords = 'junior gamer developer python socket framework security encoding libary keyboard knowledge'.split(' ')
    mistakes = 1
    guesses_letters = []
    instructions()
    password = mode(list_of_passwords).lower()
    player_board = '_' * len(password)

    while mistakes < 7:
        if password == 'exit':
            print('Goodbye.\n'
                  'Go, covered with disgrace...')
            break
        if player_board.isalpha():
            win(password)
            break
        print('\n',player_board)
        letter = player_guess(guesses_letters)

        if len(letter) == 1:
            player_board = check_letter(password, guesses_letters)
            if letter in player_board:
                continue
            else:
                error(mistakes)
                mistakes += 1
        elif len(letter) == len(password):
            if guess_word(letter, password) == True:
                win(password)
                break
            else:
                error(mistakes)
                mistakes += 1
        else:
            print('\nYou\'re probably type wrong amount of letters.\n'
              'Please, try again.')

game()