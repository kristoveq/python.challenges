from pathlib import Path

current = Path.cwd() / 'additional_materials'

def drawing_sketch():
    result = {}
    for one in Path.glob(current, 'sketch*'):
        draw = []
        with open(one, mode='r', encoding='utf-8') as file:
            for line in file.readlines():
                draw.append(line)
        name = one.stem
        result.setdefault(name, ''.join(draw))
    return result

def taking_description():
    result = {}
    for one in Path.glob(current, 'description*'):
        write = []
        with open(one, mode='r', encoding='utf-8') as file:
            for line in file.readlines():
                write.append(line)
        name = one.stem
        result.setdefault(name, ''.join(write))
    return result

def check_type_and_range(value, options):
    if not value.isdigit():
        print('Use numpad.')
        return False
    else:
        int_value = int(value)
        if int_value in range(1, len(options)+1):
            return int_value
        else:
            print('Out of range.')
            return False