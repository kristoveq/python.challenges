import game_functions as gf

class Room:

    def __init__(self, name, marker, items, specials, sketch, description):
        self.name = name
        self.marker = marker
        self.items = items
        self.specials = specials
        self.sketch = sketch
        self.description = description

    def show_room(self):
        print(self.sketch)

    def show_description(self):
        print(self.description)

    def remove_item(self, item):
        self.items.remove(item)

    def open_door(self):
        opening = 0
        for special in self.specials:
            if self.specials[special] == True:
                opening += 1
        if opening == len(self.specials):
            return True
        else:
            return False

    def final(self):
        self.show_room()
        print()
        self.show_description()

        options = ['1. Keep quiet and try to look around.']
        if 'axe' in player.items:
            options.append('2. There\'s no time to loose! Attack beast with axe!')
        print()
        for one in options:
            print(one)

        decision = False
        while decision == False:
            choice = input('What are you planning to do?')
            decision = gf.check_type_and_range(choice, options)
        if decision == 1:
            print('\nIt seems that being quiet is not your forte.\n'
                  'The beast woke up instantly, fixed you with an icy stare\n'
                  'and offered to spare your life for answering one riddle.\n'
                  'It\'s too late to fight now...\n')
            print('The one who creates me doesn\'t need me when he does.\n'
                  'The one who buys me doesn\'t need me for himself.\n'
                  'Whoever uses me will not know it.\n'
                  'What am I?\n')
            answer = input('What is your answer? :   ')
            if answer.lower() == 'coffin':
                print('You WIN life in prison with werewolf :)')
            else:
                print('The werewolf lazily pulled himself up and stood up.\n'
                      'His next move was so fast that you only had time to think about the axe,\n'
                      'see his terrible teeth and smell his awful breath...\n'
                      'Unfortunately - you died.')
        else:
            print('\nAs soon as you drew the axe, you realized that your brave heart\n'
                  'has nothing to do with the speed of the beast.\n'
                  'Unfortunately - you died.')


class Player(Room):

    def __init__(self, marker, items):
        super().__init__(None, marker, items, None, None, None)

    def show_items(self):
        if len(player.items) == 0:
            print('Your don\'t have any items.')
        else:
            for pos, item in enumerate(player.items):
                print(pos + 1, ':  ', item)

    def pick_up_item(self):
        current_position = player.current_position()
        if len(current_position.items) > 0:
            for item in current_position.items:
                picked_up = player.current_position().items.pop(0)
                player.items.append(picked_up)
                print(f'The {picked_up} has been taken.')
        else:
            print('There\'s nothing to take.')

    def use_item(self):
        if len(player.items) == 0:
            print('You don\'t have anything to use.')
        if player.current_position().marker != 1:
            print('There\'s nothing to use here.')
        else:
            for item in player.items:
                if item == 'bone key':
                    print('You used "bone key" on keyhole.')
                    player.items.remove('bone key')
                    center_room.specials['keyhole_1'] = True
                elif item == 'marble key':
                    print('You used "marble key" on keyhole.')
                    player.items.remove('marble key')
                    center_room.specials['keyhole_2'] = True
                else:
                    print('You don\'t have antyhing to use here.')

    def move(self):
        min_room_limit = 0
        max_room_limit = 2
        print('1. Go left.\n'
              '2. Go right.')

        while True:
            direction = input('Where do You want to go? : ')
            try:
                direction = int(direction)
            except ValueError:
                print('Use numpad.')

            if not direction in range(1,3):
                print('Choice out of range!')
                continue

            if (direction == 1 and player.marker - 1 < min_room_limit) \
                    or (direction == 2 and player.marker + 1 > max_room_limit):
                print('Oooops! You don\'t wanna hit a wall...isn\'t You ?')
            else:
                if direction == 2:
                    player.marker = player.marker + 1
                else:
                    player.marker = player.marker - 1
            break

    def current_position(self):
        list_of_rooms = [center_room, left_room, right_room]
        for room in list_of_rooms:
            if room.marker == player.marker:
                current_position = room
        return current_position

    def menu(self):
        possibilities = ['1. Move.', '2. Show items.', '3. Take item.', '4. Use item.', '5. Show room map.',
                         '6. Describe area.']
        if (center_room.open_door() == True) and (player.current_position().marker == center_room.marker) :
            possibilities.append('7. Go through opened DOOR.')
            print('****    Door is open!    ****')


        for one in possibilities:
            print(one)
        action = input('What do You want to do? :  ')
        result = gf.check_type_and_range(action, possibilities)
        return result


center_room = Room('--- Center room ---', 1, ['bone key'], {'keyhole_1' : False, 'keyhole_2' : False},
                   gf.drawing_sketch()['sketch_center_room'], gf.taking_description()['description_center_room'])
left_room = Room('--- Left room ---', 0, ['marble key'], None, gf.drawing_sketch()['sketch_left_room']
                 ,gf.taking_description()['description_left_room'])
right_room = Room('--- Right room ---', 2, ['axe'], None, gf.drawing_sketch()['sketch_right_room'],
                  gf.taking_description()['description_right_room'])
front_room = Room('--- Front room ---', 3, None, None, gf.drawing_sketch()['sketch_front_room'],
                  gf.taking_description()['description_front_room'])

player = Player(1, [])