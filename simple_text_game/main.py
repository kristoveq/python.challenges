import game_classes as gc

def main():

    while True:
        current_position = gc.player.current_position()
        print(gc.player.current_position().name)
        player_action = gc.player.menu()
        if player_action == 1:
            gc.player.move()
        elif player_action == 2:
            gc.player.show_items()
        elif player_action == 3:
            gc.player.pick_up_item()
        elif player_action == 4:
            gc.player.use_item()
        elif player_action == 5:
            gc.player.current_position().show_room()
        elif player_action == 6:
            gc.player.current_position().show_description()
        elif player_action == 7:
            gc.front_room.final()
            break
        print()

if __name__ == '__main__':
    main()