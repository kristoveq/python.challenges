def convert_cel_to_far(temp_c):
    farenheit = float(temp_c) * 9/5 + 32
    return farenheit

def convert_far_to_cel(temp_f):
    celsius = (float(temp_f) - 32) * 5/9
    return celsius

temp_f = input('Enter a temperature in degrees F: ')
temp_c = convert_far_to_cel(float(temp_f))
print(f'{temp_f} degrees F = {temp_c:.2f} degrees C')

temp_c = input('Enter a temperature in degrees C: ')
temp_f = convert_cel_to_far(float(temp_c))
print(f'{temp_c} degrees C = {temp_f:.2f} degrees F')