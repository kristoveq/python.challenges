universities = [
['California Institute of Technology', 2175, 37704],
['Harvard', 19627, 39849],
['Massachusetts Institute of Technology', 10566, 40732],
['Princeton', 7802, 37000],
['Rice', 5879, 35551],
['Stanford', 19535, 40569],
['Yale', 11701, 40500]
]

def enrollment_stats(list):
    student_enrollment = []
    tuition_fees = []
    for university in list:
        student_enrollment.append(university[1])
        tuition_fees.append(university[2])
    return student_enrollment, tuition_fees

students, tuition = enrollment_stats(universities)

def mean (list):
    all = sum(list)
    divider = len(list)
    result = all / divider
    return result

def median(list):
    list.sort()
    if len(list) % 2 == 0:
        first_value = (len(list) - 1) // 2
        second_value = (len(list) + 1) // 2
        result = (list[first_value] + list[second_value]) / 2
        return result
    else:
        mid_value = len(list) // 2
        result = list[mid_value]
        return result


print('*' * 30)
print(f'Total students:\t  {sum(students):,}')
print(f'Total tuition:\t ${sum(tuition):,}\n')
print(f'Student mean:\t  {mean(students):,.2f}')
print((f'Student median:\t  {median(students):,}\n'))
print(f'Tuition mean:\t ${mean(tuition):,.2f}')
print(f'Tuition median:\t ${median(tuition):,}')
print('*' * 30)