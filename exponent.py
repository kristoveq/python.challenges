base = input('Enter a base: ')
exponent = input('Enter an exponent: ')

base = float(base)
exponent = float(exponent)

print(f'{base} to the power of {exponent} = {base ** exponent}')