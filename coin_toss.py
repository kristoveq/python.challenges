import random

def coin_toss():
    heads = 0
    tails = 0
    counter = 0
    while heads == 0 or tails == 0:
        throw = random.randint(0, 1)
        counter += 1
        if throw == 0:
            heads += 1
        else:
            tails += 1
    return counter

all_trials = 0
for trial in range (10000):
    all_trials += coin_toss()

result = all_trials / 10000
print(f'Average of results in fair flipping coin is {result}.')
