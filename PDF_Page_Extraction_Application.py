import easygui as gui
from PyPDF2 import PdfWriter, PdfReader

pdf_path =  gui.fileopenbox(
    title="Select a file for extraction",
    default="*.pdf"
)
if pdf_path is None:
    exit()

start_position = gui.enterbox(
    title="Beginning of extraction",
    msg="Enter starting page number"
)
if start_position is None:
    exit()
while int(start_position) < 1:
    gui.msgbox(
        title="WARNING",
        msg="Choose only positive integer",
        ok_button="Try again"
    )
    start_position = gui.enterbox(
        title="Beginning of extraction",
        msg="Enter starting page number"
    )
start_position = int(start_position)

end_position = gui.enterbox(
    title="Ending of extraction",
    msg="Enter ending page number"
)
if end_position is None:
    exit()
while int(end_position) < start_position:
    gui.msgbox(
        title="WARNING",
        msg="Choose greater or equal number than start page",
        ok_button="Try again"
    )
    end_position = gui.enterbox(
        title="Ending of extraction",
        msg="Enter ending page number"
    )
end_position = int(end_position)

target_path = gui.filesavebox(
    title="Select location to save file",
    default="*.pdf"
)
if target_path is None:
    exit()
while pdf_path == target_path:
    gui.msgbox(
        title="WARNING",
        msg="Cannot overwrite file!",
        ok_button="Try again"
    )
    target_path = gui.filesavebox(
        title="Select location to save file",
        default="*.pdf"
    )

input_pdf = PdfReader(pdf_path)
pdf_writer = PdfWriter()
for page in input_pdf.pages[start_position : end_position]:
    pdf_writer.add_page(page)

with open(target_path, "wb") as output_file:
    pdf_writer.write(output_file)