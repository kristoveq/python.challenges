from pathlib import Path
from PyPDF2 import PdfWriter, PdfReader

pdf_path = Path.cwd() / 'scrambled.pdf'
output_path = Path.cwd() / 'unscrambled.pdf'
pdf_reader = PdfReader(str(pdf_path))
pdf_writer = PdfWriter()

counter = 1
while len(pdf_writer.pages) < 7:
    for page in pdf_reader.pages:
# page rotating
        rotation = page['/Rotate']
        if rotation < 0:
            position = rotation - (rotation * 2)
        elif rotation > 0:
            position = 360 - rotation
        else:
            position = rotation
        page.rotate(position)
# page sorting
        text = page.extract_text().strip()
        if int(text) == counter:
            pdf_writer.add_page(page)
            counter += 1

with output_path.open(mode='wb') as output_file:
    pdf_writer.write(output_file)



