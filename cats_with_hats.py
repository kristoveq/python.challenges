cats = {}
for num in range (1, 100+1):
    cats.setdefault(f'cat_{num}', 'no hat')

def wearing_hats(cats):
    num = 1
    while num <= 100:
        list_of_cats = list(cats.keys())
        for cat in list_of_cats:
            position = list_of_cats.index(cat) + 1
            if position % num == 0:
                if cats[cat] == 'no hat':
                    cats[cat] = 'HAT'
                else:
                    cats[cat] = 'no hat'
            else:
                continue
        num += 1

wearing_hats(cats)
for one in cats.items():
    print(one)
